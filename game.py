import tkinter as tk


# Class that represents a game of Tic Tac Toe
class TicTacToe:
    # initialize the game
    def __init__(self):
        '''
        The function is the constructor for a class and initializes an object with initial variables, set board and launch game loop.
        '''
        # Create a window for the game
        self.root = tk.Tk()
        # Set the title of the window
        self.root.title('Tic Tac Toe')
        self.board_size = 3
        self.play_mode = True
        # Initialize the board with empty cells represented by spaces ' '
        self.board = [[' ' for _ in range(self.board_size)] for _ in range(self.board_size)]
        # Determine whose turn it is
        self.current_turn = self.determine_turn()
        # Create a list to store buttons
        self.buttons = []
        # Create a label to display player's turn
        self.label = tk.Label(self.root, text='Player\'s turn (X)')
        # Place the label at the bottom of the board
        self.label.grid(row=self.board_size, columnspan=self.board_size)
        # Create the game board
        self.create_board()
        self.print_board()
        # Start the game loop
        self.root.mainloop()

    def create_board(self):
        '''
        The function creates a board for a game.
        '''
        # Generate the grid of buttons to represent the game board
        for i in range(self.board_size):
            row_buttons = []
            for j in range(self.board_size):
                # Create buttons for each cell in the grid
                button = tk.Button(self.root, text=' ', width=6, height=3, command=lambda row=i, col=j: self.on_click(row, col))
                button.grid(row=i, column=j)
                row_buttons.append(button)
            self.buttons.append(row_buttons)

        # Add a button to start a new game
        new_game_button = tk.Button(self.root, text='New Game', command=self.restart_game)
        new_game_button.grid(row=self.board_size + 1, columnspan=self.board_size)

    def print_board(self):
        '''
        The function prints the current state of a board.
        '''
        line = ' ---'
        # print the top line of the board
        print(line * self.board_size)
        # print cells and dividing lines
        for row in self.board:
            print('| ' + ' | '.join(row) + ' |')
            print(line * self.board_size)

    def determine_turn(self):
        '''
        The function determines whose turn it is in a game.
        '''
        count_x = sum(row.count('X') for row in self.board)
        count_o = sum(row.count('O') for row in self.board)
        # The player's turn to start the game usually starts with 'X'
        if count_x <= count_o:
            return 'X'  # turn 'X'
        else:
            return 'O'  # turn 'O'

    def restart_game(self):
        '''
        The restart_game function is used to restart the game.
        '''
        self.play_mode = True
        self.board = [[' ' for _ in range(self.board_size)] for _ in range(self.board_size)]
        # Reset the text on the buttons
        for i in range(self.board_size):
            for j in range(self.board_size):
                self.buttons[i][j].config(text=' ', state=tk.NORMAL)
        self.current_turn = 'X'
        self.label.config(text='Player\'s turn (X)')

    def check_winner(self):
        '''
        The function checks if there is a winner in a game.
        '''
        # Check horizontal lines
        for row in self.board:
            if row.count(row[0]) == len(row) and row[0] != ' ':
                return row[0]  # returns the winner
        # Check vertical lines
        for col in range(len(self.board[0])):
            if all(self.board[row][col] == self.board[0][col] and self.board[row][col] != ' ' for row in range(len(self.board))):
                return self.board[0][col]  # returns the winner
        # Check diagonals
        if self.board[0][0] == self.board[1][1] == self.board[2][2] != ' ' or self.board[0][2] == self.board[1][1] == self.board[2][0] != ' ':
            return self.board[1][1]  # returns the winner
        # Check for a draw
        if all(cell != ' ' for row in self.board for cell in row):
            return 'Draw'  # returns 'Draw' if all cells are occupied and there is no winner
        # If there is no winner and the game is not over, returns None
        return None

    def check_status(self):
        '''
        The function is used to check the status of the game.
        '''
        winner = self.check_winner()
        # Print the current state of the board
        self.print_board()

        # If there is a winner or a draw
        if winner:
            if winner == 'Draw':
                print('Draw')
                return { 'status': True, 'winner': 'Draw' }  # if there is no winner, but the game is over in a draw
            else:
                print(f'Winner: {winner}')
                return { 'status': True, 'winner': winner }  # if there is a winner

        # if there is no winner and the game is not over
        print('The game continues')
        return { 'status': False, 'winner': None }

    def player_move(self, row, col):
        '''
        The function handles the player's move in the game.

        :param row: The row parameter represents the row index of the clicked cell in a grid or table
        :param col: The col parameter represents the column index of the clicked cell in a grid or table
        '''
        # Check if the clicked cell is empty and the game is ongoing
        self.board[row][col] = self.current_turn
        button = self.buttons[row][col]
        button.config(text=self.current_turn)

    def minimax(self, depth, is_maximizing):
        '''
        The minimax function is a recursive algorithm that determines the best move for a player in a
        game by evaluating all possible moves and their outcomes.

        :param depth: The depth parameter represents the current depth of the search tree. It is used to
        control how deep the algorithm should search in the tree before returning a value
        :param is_maximizing: A boolean value indicating whether it is the maximizing player's turn or
        not. If it is the maximizing player's turn, the value is True. Otherwise, it is False
        '''
        winner = self.check_winner()

        # if 'X' wins, return a higher score
        if winner == 'X':
            return 10 - depth
        # if 'O' wins, return a lower score
        elif winner == 'O':
            return depth - 10
        # if it's a draw, return neutral score
        elif winner == 'Draw':
            return 0

        # if it's 'X' turn
        # it aims to maximize score, which is associated with a win
        # chooses moves that lead to higher scores
        if is_maximizing:
            best_score = -1000
            # Loop through each cell in the board
            for row in range(len(self.board)):
                # Loop through each cell in the row
                for col in range(len(self.board[0])):
                    # Check if the cell is empty
                    if self.board[row][col] == ' ':
                        self.board[row][col] = 'X' # makes a hypothetical move for 'X'
                        score = self.minimax(depth + 1, False) # calculates the score for this move
                        self.board[row][col] = ' ' # undo the move
                        best_score = max(score, best_score) # updates the best score
            return best_score # returns the best score
        # if it's 'O' turn
        # it aims to minimize score, which is associated with a loss
        # chooses moves that lead to lower scores
        else:
            best_score = 1000
            # Loop through each cell in the board
            for row in range(len(self.board)):
                # Loop through each cell in the row
                for col in range(len(self.board[0])):
                    # Check if the cell is empty
                    if self.board[row][col] == ' ':
                        self.board[row][col] = 'O' # makes a hypothetical move for 'O'
                        score = self.minimax(depth + 1, True) # calculates the score for this move
                        self.board[row][col] = ' ' # undo the move
                        best_score = min(score, best_score) # updates the best score
            return best_score # returns the best score

    def computer_move(self):
        '''
        The function is used to determine the computer's next move in a game.
        '''
        # Initialize the best score to a value that indicates the worst possible score for the computer
        best_score = -1000
        # Initialize the best move to an invalid value, to be updated later
        best_move = (-1, -1)

        # Loop through each cell in the board
        for row in range(len(self.board)):
            # Loop through each cell in the row
            for col in range(len(self.board[0])):
                # Check if the cell is empty
                if self.board[row][col] == ' ':
                    self.board[row][col] = 'O' # makes a hypothetical move for 'O'
                    score = self.minimax(0, False) # calculates the score for this move
                    self.board[row][col] = ' ' # undo the move

                    # Update the best score and best move if the current score is better than the previous best score
                    if score > best_score:
                        best_score = score
                        best_move = (row, col)

        return best_move # returns the best move chosen by the computer

    def make_computer_move(self):
        '''
        The function is used to make a move for the computer player in the game.
        '''
        # Get the best move for the computer
        computer_row, computer_col = self.computer_move()
        self.board[computer_row][computer_col] = 'O'
        self.buttons[computer_row][computer_col].config(text='O')

    def on_click(self, row, col):
        '''
        The function is called when a button is clicked and takes the row and column as parameters.

        :param row: The row parameter represents the row index of the clicked cell in a grid or table.
        It is used to identify the specific row that was clicked
        :param col: The `col` parameter represents the column index of the clicked cell in a grid or
        table
        '''
        # Check if the clicked cell is empty and the game is ongoing
        if self.board[row][col] == ' ' and self.play_mode:
            # Make the player's move
            self.player_move(row, col)

            # Check game status after each move
            status = self.check_status()
            if status['status']:
                self.play_mode = False
                return

            # Switch player turns
            self.current_turn = 'O' if self.current_turn == 'X' else 'X'
            self.label.config(text=f'{self.current_turn}\'s turn')

            # If it's the computer's turn, make the move after a delay
            if self.current_turn == 'O':
                self.root.after(500, self.make_computer_move)

            # Check game status after each move
            status = self.check_status()
            if status['status']:
                self.play_mode = False
                return

            # Switch player turns
            self.current_turn = 'X'
            self.label.config(text='Player\'s turn (X)')

# Create a game object
game = TicTacToe()

# tnw-tic-tac-toe

Запуск гри: ```python3 game.py```

## Звіт: Гра "Хрестики-нулики" з використанням алгоритму "Minimax" на Python

### Опис реалізованих функцій:
- ```__init__(self)``` - Конструктор класу, ініціалізує гру, встановлює необхідні початкові параметри та запускає цикл гри.
- ```create_board(self)``` - Створює інтерфейс гри на ігровій дошці за допомогою кнопок.
- ```print_board(self)``` - Виводить поточний стан гри у консоль у вигляді текстової матриці.
- ```determine_turn(self)``` - Визначає, чий зараз хід в грі: 'X' або 'O'.
- ```restart_game(self)``` - Перезапускає гру з початковими налаштуваннями.
- ```check_winner(self)``` - Перевіряє, чи є переможець у грі.
- ```check_status(self)``` - Перевіряє загальний статус гри: чи є переможець, чи ні, чи гра продовжується далі.
- ```player_move(self, row, col)``` - Обробляє хід гравця.
- ```minimax(self, depth, is_maximizing)``` - Алгоритм "Minimax" для визначення найкращого ходу комп'ютера.
- ```computer_move(self)``` - Визначає найкращий хід для комп'ютера.
- ```make_computer_move(self)``` - Здійснює хід комп'ютера після визначення найкращого варіанту.
- ```on_click(self, row, col)``` - Обробляє клік по кнопці на ігровій дошці.

### Алгоритм "Minimax":
"Minimax" - це алгоритм, який використовується в теорії ігор для прийняття рішень в стратегічних іграх, він визначає найкращий хід для гравця, який максимізує його вигоду, або найгірший хід для супротивника, який мінімізує вигоду останнього. Алгоритм рекурсивно перебирає всі можливі стани гри, щоб визначити оптимальний хід.

### Результати тестування гри:
Гравець робить свій хід, після чого комп'ютер робить оптимальний хід відповідно до алгоритму "Minimax". Гра завершується перемогою одного з гравців або нічиєю.

## Висновки:
Гра "Хрестики-нулики" є класичною грою, де кожен гравець намагається виграти або зіграти в нічию.
Даний код реалізує просту версію гри "Хрестики-нулики" з можливістю грати проти комп'ютера, який використовує алгоритм "Minimax" для своїх ходів.
Алгоритм "Minimax" є достатньо ефективним для визначення найкращого ходу в стратегічних іграх, проте може бути витратним при виконанні для більш складних ігор.
